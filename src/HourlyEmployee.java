
public class HourlyEmployee extends staffMember{
    private double  hoursWorked;
    private double rate;

    public HourlyEmployee(int id, String name, String address, double hoursWorked, double rate) {
        super(id, name, address);
        this.hoursWorked = hoursWorked;
        this.rate = rate;
    }

    public double getHoursWorked() {
        return hoursWorked;
    }

    public double getRate() {
        return rate;
    }

    public void setHoursWorked(Double hoursWorked) {
        this.hoursWorked = hoursWorked;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    @Override
    public String toString() {
        String st = "ID :"+id+"\n"+"Name :"+name+"\n"+"Address :"+address+"\n"+"Hours Worked :"+
                hoursWorked+"\n"+"Rate :"+rate +"\n"+"Payment :"+pay()+"\n";
        return st;
    }

    @Override
    public double pay() {
        return this.hoursWorked*this.rate;
    }
}
