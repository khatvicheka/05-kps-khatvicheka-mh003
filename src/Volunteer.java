
public class Volunteer extends staffMember{

    public Volunteer(int id, String name, String address) {
        super(id, name, address);
    }

    @Override
    public String toString() {
        String st = "ID :"+id+"\n"+"Name :"+name+"\n"+"Address :"+address+"\n";
        return st;
    }

    @Override
    public double pay() {
        return 0.0;
    }
}
