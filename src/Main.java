import java.util.*;
public class Main {
    public static void main(String[] args) {
        staffMember vt = new Volunteer(1,"Dara","PP");
        staffMember se = new SalariedEmployee(2,"Banana","BMC",500,150);
        staffMember he = new HourlyEmployee(3,"Vita","KPS",8,10.00);
        ArrayList<staffMember> list = new ArrayList<staffMember>();
        list.add(vt);
        list.add(se);
        list.add(he);
        Process obj = new Process();
        obj.display(list);
        obj.Menu1(list);

    }
}
