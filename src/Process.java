import org.apache.log4j.*;
import org.nocrala.tools.texttablefmt.*;

import java.io.IOException;
import java.util.*;

public class Process {
    // Method for input only numbers
    public  int validationNumber(String st){
        Scanner sc = new Scanner(System.in);
        int num=0;
        while (true){
            System.out.print(st);
            String str = sc.nextLine();
            if (str.matches("[0-9][0-9]*")){
                num = Integer.parseInt(str);
                break;
            }
            else
                System.out.println("\nPLEASE INPUT NUMBERS\n");
        }
        return num;
    }
    // End Method for input only numbers

    // Method for input Double and integer
    public  double validationDouble(String st){
        Scanner sc = new Scanner(System.in);
        double num=0;
        while (true){
            System.out.print(st);
            String str = sc.nextLine();
            if (str.matches("\\d+(\\.\\d+)?")){
                num = Double.parseDouble(str);
                break;
            }else if(str.matches("[0-9][0-9]*")){
                num = Integer.parseInt(str);
                break;
            }
            else
                System.out.println("\nPLEASE INPUT NUMBERS\n");
        }
        return num;
    }
    // End Method for input Double and integer

    // Method for input only String
    public  String validationString(String st){
        Scanner sc = new Scanner(System.in);
        String values;
        while (true){
            System.out.print(st);
            String str = sc.nextLine();
            //^(.*\s+.*)+$
            if (str.matches("[a-zA-Z]+\\.?")){
                values =str;
                break;
            }
            else
                System.out.println("\nPLEASE INPUT STRING WITH NOT WHITESPACE\n");
        }
        return values;
    }


    //End Method for input only String

    // method for press enter to continue...
    public  void promptEnterKey(){
        System.out.println("Press \"ENTER\" to continue...");
        try {
            int read = System.in.read(new byte[2]);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    // End method for press enter to continue...

    // Method display data in table
    public  void display( ArrayList<staffMember> list){
        // code for sorting object in arrayList
        Collections.sort(list, new Comparator<staffMember>() {
            @Override
            public int compare(staffMember s1, staffMember s2) {
                return (s1.getName().toUpperCase()).compareTo(s2.getName().toUpperCase());
            }
        });
        // Create Table
        CellStyle cs = new CellStyle(CellStyle.HorizontalAlign.left, CellStyle.AbbreviationStyle.crop,CellStyle.NullStyle.emptyString);
        Table t = new Table(7, BorderStyle.UNICODE_DOUBLE_BOX, ShownBorders.ALL,false,"");
        Logger.getRootLogger().setLevel(Level.OFF);
        //Create header table
        t.addCell("ID", cs);
        t.addCell("NAME", cs);
        t.addCell("ADDRESS", cs);
        t.addCell("SALARY OR HOUR", cs);
        t.addCell("BONUS OR RATE", cs);
        t.addCell("PAYMENT", cs);
        t.addCell("EMPLOYEE TYPE", cs);
        // table body
        for (staffMember s : list){
            if (s instanceof Volunteer){
                t.addCell(String.valueOf(s.getId()), cs);
                t.addCell(s.getName(), cs);
                t.addCell(s.getAddress(), cs);
                t.addCell("0 $", cs);
                t.addCell("0 $", cs);
                t.addCell("0 $", cs);
                t.addCell("VOLUNTEER", cs);
            }else if(s instanceof SalariedEmployee){
                t.addCell(String.valueOf(s.getId()), cs);
                t.addCell(s.getName(), cs);
                t.addCell(s.getAddress(), cs);
                t.addCell(String.valueOf(((SalariedEmployee) s).getSalary())+" $", cs);
                t.addCell(String.valueOf(((SalariedEmployee) s).getBonus())+" $", cs);
                t.addCell(String.valueOf(((SalariedEmployee) s).pay())+" $", cs);
                t.addCell("SALARY EMPLOYEE", cs);
            }else {
                t.addCell(String.valueOf(s.getId()), cs);
                t.addCell(s.getName(), cs);
                t.addCell(s.getAddress(), cs);
                t.addCell(String.valueOf(((HourlyEmployee) s).getHoursWorked()), cs);
                t.addCell(String.valueOf(((HourlyEmployee) s).getRate()) +" $", cs);
                t.addCell(String.valueOf(((HourlyEmployee) s).pay())+" $", cs);
                t.addCell("HOURLY EMPLOYEE", cs);
            }
        }
        //print table
        System.out.println(t.render());

    }
    // Method add staff member type Volunteer
    public void addVT(ArrayList<staffMember> list) {
        System.out.println("===========|*ADD INFORMATION*|==============");
        int idVt = validationNumber("=> Enter Staff Member's ID : ");
        String nameVt = validationString("=> Enter Staff Member's Name : ");
        String addressVt = validationString("=> Enter Staff Member's Address : ");
        list.add(new Volunteer(idVt, nameVt, addressVt));
        System.out.println("============================================");
        display(list);
    }
    // End Method add staff member type Volunteer


    // Method add staff member type SalariedEmployee
    public void addSE(ArrayList<staffMember> list){
        System.out.println("===========|*ADD INFORMATION*|==============");
        int idSE = validationNumber("=> Enter Staff Member's ID : ");
        String nameSE = validationString("=> Enter Staff Member's Name : ");
        String  addressSE = validationString("=> Enter Staff Member's Address : ");
        double salarySE = validationDouble("Enter Staff Member's Salary : ");
        double bonusSE = validationDouble("Enter Staff Member's Bonus : ");
        list.add(new SalariedEmployee(idSE,nameSE,addressSE,salarySE,bonusSE));
        System.out.println("============================================");
        display(list);
    }
    // End Method add staff member type SalariedEmployee


    // Method add staff member type HourlyEmployee
    public void addHE(ArrayList<staffMember> list){
        System.out.println("============================================");
        int idHE = validationNumber("=> Enter Staff Member's ID : ");
        String nameHE = validationString("=> Enter Staff Member's Name : ");
        String  addressHE = validationString("=> Enter Staff Member's Address : ");
        double HourHE = validationDouble("Enter Staff Member's Hour : ");
        double rateHE = validationDouble("Enter Staff Member's Rate : ");
        list.add(new HourlyEmployee(idHE,nameHE,addressHE,HourHE,rateHE));
        System.out.println("============================================");
        display(list);
    }
    //End Method add staff member type HourlyEmployee

    // Method edit staff member in arrayList
    public void Edit(ArrayList<staffMember> list){
        int idEdit = validationNumber("=> Enter Staff Member's ID for edit : ");
        for (staffMember s : list){
            if (s.getId()==idEdit){
                System.out.println(s);
                int i = validationNumber("=> Are you sure to update this recode? 1.Yes, 0.No : ");
                if (i==1){
                    if(s instanceof Volunteer){
                        String nameVt = validationString("=> Enter Staff Member's Name : ");
                        String  addressVt = validationString("=> Enter Staff Member's Address : ");
                        s.setName(nameVt);
                        s.setAddress(addressVt);
                    }
                    else if(s instanceof SalariedEmployee){
                        String nameSE = validationString("=> Enter Staff Member's Name : ");
                        String  addressSE = validationString("=> Enter Staff Member's Address : ");
                        double salarySE = validationDouble("Enter Staff Member's Salary : ");
                        double bonusSE = validationDouble("Enter Staff Member's Bonus : ");
                        s.setName(nameSE);
                        s.setAddress(addressSE);
                        ((SalariedEmployee) s).setSalary(salarySE);
                        ((SalariedEmployee) s).setBonus(bonusSE);
                    }
                    else {
                        String nameHE = validationString("=> Enter Staff Member's Name : ");
                        String  addressHE = validationString("=> Enter Staff Member's Address : ");
                        double HourHE = validationDouble("Enter Staff Member's Hour : ");
                        double rateHE = validationDouble("Enter Staff Member's Rate : ");
                        s.setName(nameHE);
                        s.setAddress(nameHE);
                        ((HourlyEmployee) s).setHoursWorked(HourHE);
                        ((HourlyEmployee) s).setRate(rateHE);
                    }
                    System.out.println("Updated successfully!");
                    promptEnterKey();
                }

            }
        }
        display(list);
    }
    //End Method edit staff member in arrayList

    // Method Remove staff member from arrayList
    public void Remove(ArrayList<staffMember> list){
        int idRemove = validationNumber("=> Enter Staff Member's ID for Delete : ");
        Iterator<staffMember> itr = list.iterator();
        while (itr.hasNext()) {
            staffMember number = itr.next();
            if (number.getId() == idRemove) {
                int i = validationNumber("=> Do you want to remove? 1.Yes, 0.No : ");
                if (i==1){
                    itr.remove();
                    System.out.println("Deleted successfully!");
                    promptEnterKey();
                }

            }
        }
        display(list);

    }
    // End Method Remove staff member from arrayList

    // Method Exit program
    public  void Exit(){
        System.out.println("-> Good bye!");
        System.exit(0);
    }
    // End Method Exit program

    //   Create Menu for Add Employee
    public void Menu2(ArrayList<staffMember> list){
        while (true){
            System.out.println("============|*ADD EMPLOYEES*|===============");
            System.out.println("1).Volunteer ");
            System.out.println("2).Hourly Employee ");
            System.out.println("3).Salary Employee ");
            System.out.println("4).Back ");
            System.out.println("============================================");
            int choose = validationNumber("=> Choose Option : ");
            if(choose>0&& choose<5){
                switch (choose){
                    case 1: addVT(list);
                        break;
                    case 2: addHE(list);
                        break;
                    case 3:addSE(list);
                        break;
                    case 4: Menu1(list);
                        break;
                }
                break;
            }else
                System.out.println("Please choose option from 1-4 :");
        }

    }
    // End  Create Menu for Add Employee

    //   Create Menu for Program
    public void Menu1(ArrayList<staffMember> list){

        while (true){
            System.out.println("=================|*MENU*|===================");
            System.out.println("1).Add Employee ");
            System.out.println("2).Edit ");
            System.out.println("3).Remove ");
            System.out.println("4).Exit ");
            System.out.println("============================================");
            int choose = validationNumber("=> Choose Option : ");
            if(choose>0&& choose<5){
                switch (choose){
                    case 1: Menu2(list);
                        break;
                    case 2: Edit(list);
                        break;
                    case 3: Remove(list);
                        break;
                    case 4: Exit();
                        break;
                }
            }else
                System.out.println("Please choose option from 1-4 :");

        }
    }
    //End   Create Menu for Program
}
