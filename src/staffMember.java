
abstract class staffMember {
    protected int id;
    protected String name;
    protected String address;
    public int getId() {
        return id;
    }

    public String getAddress() {
        return address;
    }

    public String getName() {
        return name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public staffMember(int id, String name, String address){
        this.id = id;
        this.name = name;
        this.address = address;
    }

    @Override
    public String toString() {
        String st = "ID :"+id+"\n"+"Name :"+name+"\n"+"Address :"+address+"\n";
        return st;
    }

    public abstract double pay();
}
